using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    /// <summary>
    /// 길들의 타입
    /// </summary>
    public enum E_EnvirementType
    {
        Grass = 0,
        Road,
        Water,

        Max
    }
    /// <summary>
    /// 연속된 길들의 타입
    /// </summary>
    public enum E_LastRoadType
    {
        Grass = 0,
        Road,

        Max
    }

    /// <summary>
    /// 맵의 배열
    /// </summary>
    //public GameObject[] ObjectArray;
    [Header("# 복제용 길")]
    public Road DefaultRoad = null;
    public Water WaterRoad = null;
    public ObjSpawn GrassRoad = null;

    public Transform ParentTransform = null;

    /// <summary>
    /// 맵의 갯수 최소
    /// </summary>
    public int MinPosZ = -10;
    /// <summary>
    /// 최대
    /// </summary>
    public int MaxPosZ = 10;


    public int FrontOffsetPosZ = 10;
    public int BackOffsetPosZ = 10;

    private void Start()
    {
        DefaultRoad.gameObject.SetActive(false);
        WaterRoad.gameObject.SetActive(false);
        GrassRoad.gameObject.SetActive(false);
    }

    // int p_posz = 시작하는 위치
    /// <summary>
    /// 랜덤한 길들의 그룹
    /// </summary>
    public int GroupRandomRoadLine(int p_posz)
    {
        int randomcount = Random.Range(1, 4);

        for (int i = 0; i < randomcount; i++)
        {
            GeneratorRoadLine(p_posz + i);
        }

        return randomcount;
    }
    /// <summary>
    /// 랜덤한 물길들의 그룹
    /// </summary>
    /// <param name="p_posz">시작위치</param>
    /// <returns></returns>
    public int GroupRandomWaterLine(int p_posz)
    {
        int randomcount = Random.Range(1, 4);

        for (int i = 0; i < randomcount; i++)
        {
            GeneratorWaterLine(p_posz + i);
        }

        return randomcount;
    }

    /// <summary>
    /// 랜덤한 잔디들
    /// </summary>
    /// <param name="p_posz"> 시작 점 </param>
    /// <returns></returns>
    public int GroupRandomGrassLine(int p_posz)
    {
        int randomcount = Random.Range(1, 2);

        for (int i = 0; i < randomcount; i++)
        {
            GeneratorGrassLine(p_posz + i);
        }

        return randomcount;
    }

    /// <summary>
    /// 길 생성용
    /// </summary>
    public void GeneratorRoadLine(int p_posz)
    {
        GameObject cloneobj = GameObject.Instantiate(DefaultRoad.gameObject);

        cloneobj.SetActive(true);

        Vector3 offsetpos = Vector3.zero;
        offsetpos.z = (float)p_posz;
        cloneobj.transform.SetParent(ParentTransform);
        cloneobj.transform.position = offsetpos;

        int randomrot = Random.Range(0, 2);
        if (randomrot == 1)
        {
            cloneobj.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        cloneobj.name = "RoadLine_" + p_posz.ToString();

        m_LineMapList.Add(cloneobj.transform);
        m_LineMapDic.Add(p_posz, cloneobj.transform);
    }
    /// <summary>
    /// 물 생성용
    /// </summary>
    public void GeneratorWaterLine(int p_posz)
    {
        GameObject cloneobj = GameObject.Instantiate(WaterRoad.gameObject);

        cloneobj.SetActive(true);

        Vector3 offsetpos = Vector3.zero;
        offsetpos.z = (float)p_posz;
        cloneobj.transform.SetParent(ParentTransform);
        cloneobj.transform.position = offsetpos;

        int randomrot = Random.Range(0, 2);
        if (randomrot == 1)
        {
            cloneobj.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }

        cloneobj.name = "WaterLine_" + p_posz.ToString();

        m_LineMapList.Add(cloneobj.transform);
        m_LineMapDic.Add(p_posz, cloneobj.transform);
    }

    /// <summary>
    /// 잔디생성용 
    /// </summary>
    public void GeneratorGrassLine(int p_posz)
    {
        GameObject cloneobj = GameObject.Instantiate(GrassRoad.gameObject);

        cloneobj.SetActive(true);

        Vector3 offsetpos = Vector3.zero;
        offsetpos.z = (float)p_posz;
        cloneobj.transform.SetParent(ParentTransform);
        cloneobj.transform.position = offsetpos;

        cloneobj.name = "GrassLine_" + p_posz.ToString();

        m_LineMapList.Add(cloneobj.transform);
        m_LineMapDic.Add(p_posz, cloneobj.transform);

    }
    protected E_LastRoadType m_LastRoadType = E_LastRoadType.Max;
    protected List<Transform> m_LineMapList = new List<Transform>();
    protected Dictionary<int, Transform> m_LineMapDic = new Dictionary<int, Transform>();
    protected int m_LastLinePos = 0;

    protected int m_MinLine = 0;

    /// <summary>
    /// 한번에 지울 라인 갯수
    /// </summary>
    public int m_DeleteLine = 1;    
    public int m_BackOffsetLineCount = 10;

    /// <summary>
    /// 캐릭터 기준 위치로 만들어지는 맵 관리
    /// </summary>
    public void UpdateForwardNBackRoad(int p_posz)
    {
        if(m_LineMapList.Count <= 0)
        {
            m_LastRoadType = E_LastRoadType.Grass;
            m_MinLine = MinPosZ;
            int i = 0;
            //초기용 라인들 세팅
            for (i = MinPosZ; i < MaxPosZ; i++)
            {
                int offsetval = 0;
                if(i < 0)
                {
                    // 잔디만 생성
                    GeneratorGrassLine(i);
                }
                else
                {
                    if(m_LastRoadType == E_LastRoadType.Grass)
                    {
                        int randomval = Random.Range(0, 2);
                        if(randomval == 0)
                        {
                            offsetval = GroupRandomWaterLine(i);
                        }
                        else
                        {
                            offsetval = GroupRandomRoadLine(i);                            
                        }
                        m_LastRoadType = E_LastRoadType.Road;
                    }
                    else
                    {
                        offsetval = GroupRandomGrassLine(i);

                        m_LastRoadType = E_LastRoadType.Grass;
                    }
                    //랜덤하게 생성
                    
                    i += offsetval - 1;
                }
            }

            m_LastLinePos = i;
        }

        // 새롭게 생성
        if(m_LastLinePos < p_posz + FrontOffsetPosZ)
        {
            int offsetval = 0;
            if (m_LastRoadType == E_LastRoadType.Grass)
            {
                int randomval = Random.Range(0, 2);
                if (randomval == 0)
                {
                    offsetval = GroupRandomWaterLine(m_LastLinePos);
                }
                else
                {
                    offsetval = GroupRandomRoadLine(m_LastLinePos);
                }
                m_LastRoadType = E_LastRoadType.Road;
            }
            else
            {
                offsetval = GroupRandomGrassLine(m_LastLinePos);

                m_LastRoadType = E_LastRoadType.Grass;
            }

            m_LastLinePos += offsetval;
        }


        // 지나갔으면 지우기

        if(p_posz - m_BackOffsetLineCount > m_MinLine - m_DeleteLine )
        {
            int count = m_MinLine + m_DeleteLine;
            for (int i = m_MinLine; i < count; i++)
            {
                RemoveLine(i);
            }
            m_MinLine += m_DeleteLine;
        }
    }

    void RemoveLine(int p_posz)
    {
        if(m_LineMapDic.ContainsKey(p_posz))
        {
            Transform transobj = m_LineMapDic[p_posz];
            GameObject.Destroy(transobj.gameObject);

            m_LineMapList.Remove(transobj);
            m_LineMapDic.Remove(p_posz);

            
        }

        else
        {
            Debug.Log("error");
        }
    }


    /* void CloneRoad(int p_posz)
    {
        //Vector3 offsetpos = new Vector3(0, 0, i * 2);
        int randomindex = Random.Range(0, ObjectArray.Length);
        GameObject cloneobj = GameObject.Instantiate(ObjectArray[randomindex]);
        cloneobj.SetActive(true);

        Vector3 offsetpos = Vector3.zero;
        offsetpos.z = (float)p_posz;
        cloneobj.transform.SetParent(ParentTransform);
        cloneobj.transform.position = offsetpos;

        int randomrot = Random.Range(0, 2);
        if (randomrot == 1)
        {
            cloneobj.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }

    }*/

}
