using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

// restart 기능을 만들자
// 재시작 할 때 image restart를 보이지 않게 하자
public class GameManager : MonoBehaviour
{


    public GameObject imageRestart;
    private int BackCount = 3;
   
    void Start()
    {
        //활성화, 비활성화
        imageRestart.SetActive(false);
    }

   
    void Update()
    {
        // DownArrow 버튼을 눌렀을때, BackCount 수 -1
        if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            --BackCount;
        }

        // BackCount 가 0이라면 restart버튼 활성화
        if(BackCount == 0)
        {
            imageRestart.SetActive(true);

        }
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Crash"))
        {
            imageRestart.SetActive(true);
            
        }

    }
   

    //restart 버튼을 누르면
    public void OnClickRestart()
    {
        //첫 장면을 가져오게 된다.
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        // BackCount 갯수 초기화
        BackCount = 4;
    }
}
