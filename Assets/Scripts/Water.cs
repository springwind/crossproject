using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    /// <summary>
    /// 복사생성될 raft 
    /// </summary>
    public Raft CloneTarget = null;
    public Transform ClonePosition = null;

    /// <summary>
    /// 스폰될 확률
    /// </summary>
    public int SpawnPersentage = 70;

    /// <summary>
    /// 복사 생성 딜레이
    /// </summary>
    public float CloneDelay = 0.2f;

    protected float DelayToClone = 0.2f;

    private void Update()
    {
        float currSec = Time.time;
        if (DelayToClone <= currSec)
        {
            int randomval = Random.Range(0, 99);
            if (randomval < SpawnPersentage)
            {

                CloneRaft();

            }
            DelayToClone = currSec + CloneDelay;
        }
    }

    /// <summary>
    /// 뗏목을 복사생성하는 메서드
    /// </summary>
    private void CloneRaft()
    {
        Transform clonepos = ClonePosition;
        Vector3 offsetpos = clonepos.position;
        offsetpos.y = 0.25f;

        GameObject cloneObj = GameObject.Instantiate(
            CloneTarget.gameObject,
            offsetpos,
            ClonePosition.rotation,
            this.transform);

        cloneObj.SetActive(true);
    }



}