using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    /// <summary>
    /// 카메라가 부드럽게 움직이게 하는 변수
    /// </summary>
    public float Smoothing = 5.0f;

    /// <summary>
    /// 카메라 이동을 위한 오프셋
    /// </summary>
    Vector3 m_OffsetVal;

    private void Start()
    {
        m_OffsetVal = transform.position - Target.position;
    }

    private void Update()
    {
        Vector3 CameraPos = Target.position + m_OffsetVal;
        transform.position = Vector3.Lerp(transform.position, CameraPos, Smoothing * Time.deltaTime);
    }
}