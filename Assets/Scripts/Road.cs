using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    /// <summary>
    /// 클론될 Car 타깃
    /// </summary>
    public Car CloneTarget = null;
    public Transform ClonePosition = null;

    /// <summary>
    /// 스폰될 확률
    /// </summary>
    public int SpawnPersentage = 50;

    /// <summary>
    /// 클론 딜레이
    /// </summary>
    public float CloneDelay = 1f;


    protected float DelayToClone = 0.2f;

    private void Update()
    {
        float currSec = Time.time;
        if( DelayToClone <= currSec)
        {
            int randomval = Random.Range(0 , 99);
            if(randomval < SpawnPersentage )
            {

                CloneCar();

            }
            DelayToClone = currSec + CloneDelay;
        }
    }

    /// <summary>
    /// 차를 클론하는 메서드
    /// </summary>
    private void CloneCar()
    {
        Transform clonepos = ClonePosition;
        Vector3 offsetpos = clonepos.position;
        offsetpos.y = 0.5f;

        GameObject cloneObj = GameObject.Instantiate(
            CloneTarget.gameObject,
            offsetpos, 
            ClonePosition.rotation, 
            this.transform);

        cloneObj.SetActive(true); 
    }

    

}
