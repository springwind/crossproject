using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    /// <summary>
    /// 차의 이동속도
    /// </summary>
    public float MoveSpeed = 1.0f;
    /// <summary>
    /// 오브젝트 파괴 거리
    /// </summary>
    public float DestroyRange = 50.0f;

    private void Update()
    {
        float moveX = MoveSpeed * Time.deltaTime;
        this.transform.Translate(moveX, 0f, 0f);

        if(this.transform.localPosition.x >= DestroyRange)
        {
            GameObject.Destroy(this.gameObject);
        }
    }


}
