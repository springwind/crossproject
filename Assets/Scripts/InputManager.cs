using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public PlayerMovement playerMovement;


    private void OnMove(InputValue value)
    {
        Vector2 direction = value.Get<Vector2>();

        if (direction.magnitude < 0.1) return;

        if(Mathf.Abs(direction.y) > 0.1)
        {
            direction.x = 0;
        }
        direction.Normalize();

        playerMovement.InputDirection(direction);
    }
}
