using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody PlayerBody = null;
    public MapManager mapManager = null;

    private void Start()
    {

        string[] templayer = new string[] { "Obj" };
        m_ObjLayerMask = LayerMask.GetMask(templayer);

        mapManager.UpdateForwardNBackRoad((int)this.transform.position.z);
    }
    public enum E_DirectionType
    {
        forward = 0,
        back,
        Left,
        Right,
    }

    protected E_DirectionType m_DirectionType = E_DirectionType.forward;
    protected int m_ObjLayerMask = -1;

    /// <summary>
    /// 바라보고 있는 방향에 물체가 있는지 RayCast를 이용하여 검사
    /// </summary>
    /// <param name="p_movetype"></param>
    /// <returns>있다면 이동 불가</returns>
    protected bool IsCheckDirectionViewMove(E_DirectionType p_movetype)
    {
        Vector3 direction = Vector3.zero;

        switch (p_movetype)
        {
            case E_DirectionType.forward:
                direction = Vector3.forward;
                break;
            case E_DirectionType.back:
                direction = Vector3.back;
                break;
            case E_DirectionType.Left:
                direction = Vector3.left;
                break;
            case E_DirectionType.Right:
                direction = Vector3.right;
                break;
            default:
                Debug.LogErrorFormat("Error");
                break;
        }
        RaycastHit hitobj;
        if(Physics.Raycast(this.transform.position, direction ,out hitobj , 1.0f, m_ObjLayerMask))
        {


            return true;
        }
        return false;
    }


    /// <summary>
    /// 레이케스트를 이용하여 앞에 오브젝트가 있는지 확인
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    protected bool IsCheckDirectionViewMove(Vector3 dir)
    {
        
        RaycastHit hitobj;
        if (Physics.Raycast(this.transform.position, dir, out hitobj, 1.0f, m_ObjLayerMask))
        {


            return true;
        }
        return false;
    }


    /// <summary>
    /// 앞에 오브젝트가 있다면 플레이어 이동 불가 없다면 이동
    /// </summary>
    /// <param name="p_movetype"></param>
    
    protected void SetPlayerMove (E_DirectionType p_movetype)
    {
        if(IsCheckDirectionViewMove(p_movetype))
        {
            return;
        }

        Vector3 offsetpos = Vector3.zero;

        switch (p_movetype)
        {
            case E_DirectionType.forward:
                offsetpos = Vector3.forward;
                break;
            case E_DirectionType.back:
                offsetpos = Vector3.back;
                break;
            case E_DirectionType.Left:
                offsetpos = Vector3.left;
                break;
            case E_DirectionType.Right:
                offsetpos = Vector3.right;
                break;
            default:
                Debug.LogErrorFormat("Error");
                break;
        }

        this.transform.position += offsetpos;
        
        m_RaftOffsetPos += offsetpos;

        mapManager.UpdateForwardNBackRoad((int)this.transform.position.z);
    }

   

    
       

    /// <summary>
    /// 캐릭터 이동관련 메서드
    /// </summary>
    /// <param name="value"></param>
    public void InputDirection(Vector2 value)
    {        
        Vector3 direction = new Vector3(value.x, 0, value.y);

        if (IsCheckDirectionViewMove(direction)) return;



        transform.position += direction;
        m_RaftOffsetPos += direction;
        mapManager.UpdateForwardNBackRoad((int)this.transform.position.z);
        
        
        
    }

    /// <summary>
    /// raft를 따라 떠내려가게 하는 메서드
    /// </summary>
    Vector3 m_RaftOffsetPos = Vector3.zero;
    protected void UpdateRaft()
    {
        if (RaftObject == null) return;



        Vector3 playerpos = RaftObject.transform.position + m_RaftOffsetPos;
        this.transform.position = playerpos;
    }

    private void Update()
    {

        
        UpdateRaft();



    }
    public GameManager gameManager;

    protected Raft RaftObject = null;
    protected Transform RaftCompareObject = null;
    public int Life = 4;
    protected void OnTriggerEnter(Collider other)
    {
        if(other.tag.Contains("Raft"))
        {
            RaftObject = other.transform.parent.GetComponent<Raft>();
            if(RaftObject != null)
            {
                RaftCompareObject = RaftObject.transform;
                m_RaftOffsetPos = this.transform.position - RaftObject.transform.position;
            }

            return;
        }

       
    }



    protected void OnTriggerExit(Collider other)
    {


        if (other.tag.Contains("Raft")
            && RaftCompareObject == other.transform.parent)
        {
            RaftCompareObject = null;
            RaftObject = null;
            m_RaftOffsetPos = Vector3.zero;
        }
                
    }

}
