using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raft : MonoBehaviour
{
    /// <summary>
    /// �¸��� �̵��ӵ�
    /// </summary>
    public float MoveSpeed = 1.0f;
    /// <summary>
    /// �¸� �ı� �Ÿ�
    /// </summary>
    public float DestroyRange = 50.0f;

    private void Update()
    {
        float moveX = MoveSpeed * Time.deltaTime;
        this.transform.Translate(moveX, 0f, 0f);

        if (this.transform.localPosition.x >= DestroyRange)
        {
            GameObject.Destroy(this.gameObject);
        }
    }


}