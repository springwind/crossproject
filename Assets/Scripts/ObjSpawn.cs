using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjSpawn : MonoBehaviour
{

    public List<Transform> EnvirmentObjectList = new List<Transform>();
    /// <summary>
    /// 오브젝트 생성 좌표 최소
    /// </summary>
    public int StartMinVal = -40;
    /// <summary>
    /// 최대
    /// </summary>
    public int StartMaxVal = 40;

    /// <summary>
    /// 스폰 레인지(확률)
    /// </summary>
    public int SpawnCreateRandom = 20;

    /// <summary>
    /// 시작지점 주변 블록생성
    /// </summary>
    public void GeneratorRoundBlock()
    {
        int randomIndex = 0;
        GameObject tempclone = null;
        Vector3 offsetpos = Vector3.zero;

        for (int i = StartMinVal; i < StartMaxVal; ++i)
        {
            if(i < -5 || i > 5)
            {
                randomIndex = Random.Range(0, EnvirmentObjectList.Count);
                tempclone = GameObject.Instantiate(EnvirmentObjectList[randomIndex].gameObject);
                offsetpos.Set(i, 1.0f, 0.0f);

                tempclone.transform.SetParent(this.transform);
                tempclone.transform.localPosition = offsetpos;
            }
        }
    }

    /// <summary>
    /// 시작지점 뒤 블록 생성
    /// </summary>
    void GeneratorBackBlock()
    {
        int randomIndex = 0;        
        GameObject tempclone = null;
        Vector3 offsetpos = Vector3.zero;

        for (int i = StartMinVal; i < StartMaxVal; ++i)
        {
            randomIndex = Random.Range(0, EnvirmentObjectList.Count);
            tempclone = GameObject.Instantiate(EnvirmentObjectList[randomIndex].gameObject);
            offsetpos.Set(i, 1.0f, 0.0f);

            tempclone.transform.SetParent(this.transform);
            tempclone.transform.localPosition = offsetpos;
        }
    }

    /// <summary>
    /// 나무 생성 관리
    /// </summary>
    public void GeneratorTree()
    {
        int randomIndex = 0;
        int randomval = 0;
        GameObject tempclone = null;
        Vector3 offsetpos = Vector3.zero;

        

        for (int i = StartMinVal; i < StartMaxVal; ++i)
        {
            randomval = Random.Range(0, 100);
            if (randomval < SpawnCreateRandom)
            {
                randomIndex = Random.Range(0, EnvirmentObjectList.Count);
                tempclone = GameObject.Instantiate(EnvirmentObjectList[randomIndex].gameObject);
                offsetpos.Set(i, 1.0f, 0.0f);

                tempclone.transform.SetParent(this.transform);
                tempclone.transform.localPosition = offsetpos;
            }
        }
    }

    /// <summary>
    /// 오브젝트를 랜덤 생성하기위한 메서드
    /// </summary>
    void GeneratorEnvirment()
    {
        if(this.transform.position.z <= -4)
        {
            GeneratorBackBlock();
        }
        else if (this.transform.position.z <= 0)
        {
            GeneratorRoundBlock();
        }
        else
        {
            GeneratorTree();
        }
    }
    private void Start()
    {
        GeneratorEnvirment();
    }
}
